provider "aws" {
  region     = "us-east-2"
  access_key = var.key_access
  secret_key = var.key_secret
}

resource "aws_instance" "my_ec2" {
  tags = {
    Name = "meddeb_ec2"
  }
  ami           = "ami-0d5bf08bc8017c83b"
  instance_type = "t2.micro"
  key_name      = "sysops"
  root_block_device { delete_on_termination = true }
  security_groups = ["${aws_security_group.allow_tomcat.name}"]
}

resource "aws_security_group" "allow_tomcat" {
  name        = "allow_tomcat"
  description = "Allow traffic for tomcat server"

  ingress {
    description = "Acces SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Acces tomcat"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tomcat"
  }
}

resource "aws_eip" "lb" {
  instance = aws_instance.my_ec2.id
  vpc      = true
  provisioner "local-exec" {
    command = "echo PUBLIC IP: ${aws_eip.lb.public_ip} ;  >> infos_ec2.txt"
  }
}
